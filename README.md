# FSE_Exercício_2

## Programa Python

Para a execução do programa python basta ir para o diretório 4_1_programa_python/ e executar
```bash
python3 main.py
```

## Programa C

Para a execução do programa C basta ir para o diretório 4_2_programa_C/ e executar

```bash
make
```