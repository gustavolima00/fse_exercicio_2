import smbus2
import bme280
from  lcd_16x2_i2c import RPi_I2C_driver
from time import *

porta_i2c = 1
endereco = 0x76
bus = smbus2.SMBus(porta_i2c)

calibracao_paramentros = bme280.load_calibration_params(bus, endereco)

mylcd = RPi_I2C_driver.lcd()
while True:
    dado = bme280.sample(bus, endereco, calibracao_paramentros)
    line_1 = f"T:{dado.temperature:.2f} U:{dado.humidity:.2f}"
    line_2 = f"P:{dado.pressure:.2f}"
    print(f"|{line_1}|")
    print(f"|{line_2}|")
    print()
    mylcd.lcd_display_string(line_1, 1)
    mylcd.lcd_display_string(line_2, 2)
    sleep(1)